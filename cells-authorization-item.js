{
  const {
    html,
  } = Polymer;
  /**
    `<cells-authorization-item>` Description.

    Example:

    ```html
    <cells-authorization-item></cells-authorization-item>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-authorization-item | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsAuthorizationItem extends Polymer.Element {

    static get is() {
      return 'cells-authorization-item';
    }

    static get properties() {
      return {
        name : String,
        description : Object,
        paymentDate : {
          type : Object,
          value : {
            label : 'Pago',
            value : '2019-02-07',
            format : 'DD-MM-YYYY'
          }
        },
        hideQuantities : {
          type : Boolean,
          value : false
        },
        primaryAmount : {
          type : Object,
          value : {
            label :  'Monto',
            currency : 'EUR',
            value : '125000'
          }
        },
        quantity : {
          type : Object,
          value : {
            label : 'Cantidad',
            value : 45
          }
        }
      };
    }

  }

  customElements.define(CellsAuthorizationItem.is, CellsAuthorizationItem);
}
